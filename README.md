# Gensim Application #
## for Business Model Crunching ##

Python based application for canvas analysis using the Tf-Idf similarity measure with cosine similarity.

##Features:
- Retrieve canvas data from cloudant database
- Use Gensim project to analyse canvas data and compare them to the input strings
- Return ranked results for the queried source and target field

## Set-Up ##
1. Click the Deploy to Bluemix button below to import this repository into your running bluemix account.
1. Change the following coding:

```
#!python
cloudantURL = "https://Username:Password@Host"
cloudantUsername = "xxxxxxx-bluemix"
cloudantPassword = "xxxxxxxxxx"
```
in beginning of the file:

```
#!javascript

welcome.py [line 30,31]

```

You can find your data here:

![CloudantDB.png](https://bitbucket.org/repo/7EKLn58/images/952229803-CloudantDB.png)


## Deployment ##

[![Deploy to Bluemix](https://bluemix.net/deploy/button.png)](http://bit.ly/2on10lt)