# Copyright 2015 IBM Corp. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from flask import Flask, jsonify, request, Response
from gensim import corpora, models, similarities

app = Flask(__name__)

@app.route('/')
def Welcome():
    return app.send_static_file('index.php')
   
@app.route('/api/getsimi/<sameMode>/<compareKey>/<sourceField>/<destField>/<inputString>')
def GetSimi(sameMode, compareKey, sourceField, destField, inputString):
    
	# Configuration parameters                                                                                                                                                                              
	queryDBSelector = {"modelType": {"$eq": "leanCanvas"}, "language": {"$eq": "english"}, "quality": {"$gt": 0.65}} 
	cloudantURL = "https://a52a0b27-971e-4ffb-b3cd-c53ee89fd078-bluemix:53d9a0d57a31f9b945d658664c9d4eb142f6be7055c6df1066e43eec7291ffad@a52a0b27-971e-4ffb-b3cd-c53ee89fd078-bluemix.cloudant.com"
	cloudantUsername = "a52a0b27-971e-4ffb-b3cd-c53ee89fd078-bluemix"
	cloudantPassword = "53d9a0d57a31f9b945d658664c9d4eb142f6be7055c6df1066e43eec7291ffad"
	leaveCardsSeparate = "yes" #leaveCardsSeparate = "yes", leaveCardsSeparate = "no"
	searchString = str(inputString)
	chosenBusinessModelCanvasField = 0 #key partners = 0, key activities = 1, value proposition = 2, customer relationships = 3, customer segments = 4, key resources = 5, channels = 6, cost structure = 7, revenue streams = 8
	amountSimilarityOutput = 7 #integer 1 to maximum
	
	#Output Design
	amountCardsToReturn = 3
	
	#=======================================================================================================================================================================================================
	
	import logging
	logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
	
	from cloudant.client import Cloudant
	from cloudant.query import Query
	
	import json
	
	from gensim import corpora, models, similarities
	
	canvases = [] #all canvases loaded from DB
	
	print ("Loading canvases from DB...")
	client = Cloudant(cloudantUsername, cloudantPassword, url=cloudantURL)
	client.connect()
	businessModelDB = client['businessmodelcatalogue']
	query = Query (businessModelDB, selector=queryDBSelector)
	for doc in query.result:
		canvases.append(doc)
	
	print (str(len(canvases))+" Canvases loaded.\n") #show amount of loaded Canvases
	
	documents = [] #text strings to fill the tf-idf corpus
	documents_id = [] #contains the canvas id (url) to have for each document the url reference
	
	for a in canvases[:]:
	    if compareKey == "fields": #native
	        preProcessedData = a  
	            
	    else:
	        preProcessedData = a[str(compareKey)] #stemmed words and others (compareKey MUST have the same synstax as the json element of the documents in the DB)
	        
	    fields_data = preProcessedData["fields"]
	    
	    if sameMode == "false":
	    	temp = sourceField
	    	sourceField = destField
	    	destField = temp
	    
	    if a["modelType"] == "leanCanvas":
	    	if sourceField == "Problem":
	    		box_data = (fields_data[0])	
	    	elif sourceField == "Solution":
	    		box_data = (fields_data[1])
	    	elif sourceField == "Key Metrics":
	    		box_data = (fields_data[2])   		
	    	elif sourceField == "Unique Value Proposition":
	    		box_data = (fields_data[3])   	
	    	elif sourceField == "Unfair Advantage":
	    		box_data = (fields_data[4])		
	    	elif sourceField == "Channels":
	    		box_data = (fields_data[5])
	    	elif sourceField == "Customer Segments":
	    		box_data = (fields_data[6])
	    	elif sourceField == "Cost Structure":
	    		box_data = (fields_data[7])	    		
	    	elif sourceField == "Revenue Stream":
	    		box_data = (fields_data[8])   		
	    	else:
	    		print ("sourceField value not chosen correctly.")
	    
	    elif a["modelType"] == "BusinessModelCanvas":
	        box_data = (fields_data[chosenBusinessModelCanvasField])
	        
	    box_data_cards = box_data["cards"]
	    boxCard = ""
	    boxCards = ""

		#concatenate titels + text of the documents an append them to the documents array; and the associated ID to the documents_id array
	    for i in box_data_cards[:]:
	        title = i["title"]
	
	        note = i["note"]
	
	        boxCard = title + " " + note

	        boxCards = boxCards + "" + title + " " + note
	        
	        if leaveCardsSeparate == "yes":
	            documents.append(boxCard)
	            documents_id.append(a["_id"])
	
	    if leaveCardsSeparate == "no":
	        documents.append(boxCards)
	        documents_id.append(a["_id"])       
	
	
	search_string = searchString.encode("utf-8")
	
	texts = [[word for word in document.lower().split()]
	    for document in documents]
	
	dictionary = corpora.Dictionary(texts)
	
	print (dictionary)
	
	search_string_pre = dictionary.doc2bow(search_string.lower().split())
	
	corpus = [dictionary.doc2bow(text) for text in texts]
	
	tfidf = models.TfidfModel(corpus, normalize=False) #calculate the tfidf values
	print(tfidf)
	index = similarities.SparseMatrixSimilarity(tfidf[corpus], num_features=4000)
	
	def getKey(item):
	    return item[1]
	
	sims = index[tfidf[search_string_pre]]
	#print ("sims: ", (enumerate(sims)))
	
	result = list(enumerate(sims))
	sortedList = sorted(result, key=getKey) #list with the cosine similarity values
	#print (sorted(result, key=getKey))
	
	result = []
	
	# second tf-idf and cosine similarity comparison
	
	if amountSimilarityOutput+1 > len(sortedList):
	    print ("Please reduce amout of similar Canvases in the output. amountSimilarityOutput is to high.")
	else:
	    counter = 1
	    documents2 = []
	    documents2ID = []
	    documentsNative = []
	    compareResult = []
	    compareMeasure = []
	    
	    # find the canvases of the cards with the highest similarity and create a new corpus filled with ALL Cards of the chosen result filed of the respective canvas
	    
	    for counter in range(1, amountSimilarityOutput+1):
	        domain, text = sortedList[-counter]
	        #print (str(counter) + ". Similarity: " + str(documents_id[int(domain)]), text)
	        compareResult.append(str(documents[int(domain)]))
	        compareMeasure.append(text)
	        for a in canvases[:]:
	        	if a["_id"] == str(documents_id[int(domain)]):
	        		nativeData = a["fields"]
	        		if compareKey == "fields":
	        			fieldsData = a["fields"]
	        		else:
	        			temp = a[str(compareKey)]
	        			fieldsData = temp["fields"]
	        			
	        		if a["modelType"] == "leanCanvas":
	        			if destField == "Problem":
	        				resultFieldData = fieldsData[0]
	        				resNativeData = nativeData[0]
	        			elif destField == "Solution":
	        				resultFieldData = fieldsData[1]
	        				resNativeData = nativeData[1]
	        			elif destField == "Key Metrics":
	        				resultFieldData = fieldsData[2]
	        				resNativeData = nativeData[2]
	        			elif destField == "Unique Value Proposition":
	        				resultFieldData = fieldsData[3]
	        				resNativeData = nativeData[3]
	        			elif destField == "Unfair Advantage":
	        				resultFieldData = fieldsData[4]
	        				resNativeData = nativeData[4]
	        			elif destField == "Channels":
	        				resultFieldData = fieldsData[5]
	        				resNativeData = nativeData[5]
	        			elif destField == "Customer Segments":
	        				resultFieldData = fieldsData[6]
	        				resNativeData = nativeData[6]
	        			elif destField == "Cost Structure":
	        				resultFieldData = fieldsData[7]
	        				resNativeData = nativeData[7]
	        			elif destField == "Revenue Stream":
	        				resultFieldData = fieldsData[8]
	        				resNativeData = nativeData[8]
	        
	       			box_data_cards = resultFieldData["cards"]
	       	
	       			boxCard = ""
	       			boxCards = ""
	       			
	       			nativeCards = resNativeData["cards"]
	       			
	       			for n in nativeCards[:]:
	       				titleNative = n["title"]
	       				noteNative = n["note"]
	       				nativeBoxCard = titleNative + " " + noteNative
	       				documentsNative.append(nativeBoxCard)
	       	
	     		  	for i in box_data_cards[:]:
	       				title = i["title"]
	       				note = i["note"]
	       				boxCard = title + " " + note
	       				#print (str(boxCard))
	       		
	 	      			if leaveCardsSeparate == "yes":
			       			documents2.append(boxCard)
			       			documents2ID.append(a["_id"])
	     		  			#print ("documents2ID appended: " + str(a["_id"]) )

	    texts2 = [[word for word in document.lower().split()] for document in documents2]
	       	
	    dictionary2 = corpora.Dictionary(texts2)
	       	
	    corpus2 = [dictionary2.doc2bow(text) for text in texts2]
	    tfidf2 = models.TfidfModel(corpus2, normalize=False)
	    print(tfidf2)
	    index2 = similarities.SparseMatrixSimilarity(tfidf2[corpus2], num_features=4000)
	    
	    counter3 = 0
	    
	    #compare each of the documents identified as similary in the first comparison with the corpus.
	    
	    for a in compareResult[:]:
	    	search_string_pre2 = dictionary2.doc2bow(a.lower().split())
	       	
	    	sims2 = index2[tfidf2[search_string_pre2]]
	    	resultsLIST = list(enumerate(sims2))
	    	sortedList2 = sorted(resultsLIST, key=getKey)
	    	print(sortedList2)
   	
	    	counter2 = 1
	       	
	    	for counter2 in range(1, len(sortedList2)):
	    		domain, text = sortedList2 [-counter2]
	    		
	    		measure = float(text)*float(0.3) + float(compareMeasure[counter3])*float(0.7)
	    		#print (measure)
	    		
	    		entry2 = {
	    			"id" : str(documents2ID[int(domain)]),
	    			"measure" : str(measure),
	    			"textSimilarity" : documentsNative[int(domain)]
	    		}
	    	
	    		result.append(entry2)
	    		counter2 += counter2
	    		
	    	counter3 += counter3
	    	
	sortedResult2 = sorted(result, key=lambda k : float(k['measure']), reverse=True)
	
	unique = []
	unique2 = []
	
	for a in sortedResult2[:]:
		alreadyImported = "no"
		for b in unique[:]:
			if a["textSimilarity"] == b["textSimilarity"]:
				alreadyImported = "yes"
			
			if a["measure"] == 0:
				alreadyImported = "yes"
		
		if alreadyImported == "yes":
			continue
		else:
			unique.append(a)
			
	counterOutput = 0
	
	for counterOutput in range (0, amountCardsToReturn):
		unique2.append(unique[counterOutput])

	print ("Verarbeitung abgeschlossen.")

	return json.dumps(unique2)

port = os.getenv('PORT', '5000')
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))
